/**
 * @file    score_analyser.h
 * @author  Saminda Wijeratne <samindaw@gatech.edu>
 * @brief   Implement serial functions required to calculate grades
 *
 * Copyright (c) 2018 Georgia Institute of Technology. All Rights Reserved.
 */


#include "score_analyser.h"
#include <stdlib.h>
#include <stdio.h>

double get_max_score(double* scores, int n){
    double tempMax = 0;
    for (int i = 0; i < n; i++) {
        if (scores[i] > tempMax) {
            tempMax = scores[i];
        }
    }
    return tempMax;
}

double get_min_score(double* scores, int n){
    double tempMin = scores[0];
    for (int i = 0; i < n; i++) {
        if (scores[i] < tempMin) {
            tempMin = scores[i];
        }
    }
    return tempMin;
}

double get_average_score(double* scores, int n){
    double tempSum = 0;
    for (int i = 0; i < n; i++) {
        tempSum += scores[i];
    }
    return tempSum / n;
}

void calculate_grades(double* scores, int n, int* grades){
    double maxScore = get_max_score(scores, n);
    double minScore = get_min_score(scores, n);
    double avgScore = get_average_score(scores, n);
    double d1Score = maxScore - avgScore;
    double d2Score = avgScore - minScore;

    for (int i = 0; i < n; i++) {
        if (avgScore + 2.0 / 3 * d1Score <= scores[i] && scores[i] <= maxScore) {
            grades[i] = 0;
        }
        else if (avgScore + 1.0 / 3 * d1Score <= scores[i]) {
            grades[i] = 1;
        }
        else if (avgScore <= scores[i]) {
            grades[i] = 2;
        }
        else if (minScore + 1.0 / 5 * d2Score <= scores[i]) {
            grades[i] = 3;
        }
        else {
            grades[i] = 4;
        }
    }
    return;
}

void calculate_grade_count(double* scores, int n, int** grade_counts, int* no_of_grades){
    int grades[n];
    calculate_grades(scores, n, grades);
    *no_of_grades = 5;

    *grade_counts = (int*) malloc(sizeof(int) * *no_of_grades);

    for (int i = 0; i < *no_of_grades; i++) {
        (*grade_counts)[i] = 0;
    }
    for (int i = 0; i < n; i++) {
        (*grade_counts)[grades[i]]++;
    }
    return;
}
