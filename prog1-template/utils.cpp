/**
 * @file    mpi_score_analyser.h
 * @author  Saminda Wijeratne <samindaw@gatech.edu>
 * @brief   Implements parallel functions required to calculate grades
 *
 * Copyright (c) 2018 Georgia Institute of Technology. All Rights Reserved.
 */


#include "utils.h"
#include <iostream>

/*********************************************************************
 *                 Implement your own functions here                 *
 *********************************************************************/

// ...

