/**
 * @file    mpi_score_analyser.c
 * @author  Saminda Wijeratne <samindaw@gatech.edu>
 * @brief   Implements parallel functions required to calculate grades
 *
 * Copyright (c) 2018 Georgia Institute of Technology. All Rights Reserved.
 */


#include "mpi_score_analyser.h"

void mpi_distribute_scores(double* all_scores, int n, double** scores, int* local_n, MPI_Comm comm){
    // Hint: Lookup the usage of MPI scatter functions
        
    // TODO
    int rank, size;
    MPI_Comm_rank(comm, &rank);
    MPI_Comm_size(comm, &size);
    if (rank == 0) {
        *local_n = n / size;
    }
    MPI_Bcast(local_n, 1, MPI_INT, 0, comm);
    (*scores) = (double*) malloc(sizeof(double) * (*local_n));
    MPI_Scatter(all_scores, *local_n, MPI_DOUBLE, *scores, *local_n, MPI_DOUBLE, 0, comm);
}

double mpi_get_max_score(double* scores, int n, MPI_Comm comm){
    // Hint: Lookup usage of functions like MPI_Scan, MPI_Reduce, 
    //       MPI_Allreduce, MPI_Bcast
    
    // TODO
    double maxScore = 0;
    for (int i = 0; i < n; i++) {
        if (scores[i] > maxScore) {
            maxScore = scores[i];
        }
    }
    double globalMax = 0;
    MPI_Allreduce(&maxScore, &globalMax, 1, MPI_DOUBLE, MPI_MAX, comm);
    return globalMax;
}

double mpi_get_min_score(double* scores, int n, MPI_Comm comm){
    // TODO
    double minScore = scores[0];
    for (int i = 0; i < n; i++) {
        if (scores[i] < minScore) {
            minScore = scores[i];
        }
    }
    double globalMin = minScore;
    MPI_Allreduce(&minScore, &globalMin, 1, MPI_DOUBLE, MPI_MIN, comm);
    return globalMin;
}

double mpi_get_average_score(double* scores, int n, MPI_Comm comm){
    // TODO
    double sum = 0;
    for (int i = 0; i < n; i++) {
        sum += scores[i];
    }
    double globalSum;
    MPI_Allreduce(&sum, &globalSum, 1, MPI_DOUBLE, MPI_SUM, comm);
    int No = 0;
    MPI_Allreduce(&n, &No, 1, MPI_INT, MPI_SUM, comm);
    return globalSum / No;
}

void mpi_calculate_grades(double* scores, int n, int* grades, MPI_Comm comm){
    // Hint: If you use mpi_get_max_score(..), mpi_get_min_score(..) and mpi_get_average_score(..) functions
    //      you'd not need to call any MPI functions inside this function
    
    // TODO
    double maxScore = mpi_get_max_score(scores, n, comm);
    double minScore = mpi_get_min_score(scores, n, comm);
    double avgScore = mpi_get_average_score(scores, n, comm);
    double d1Score = maxScore - avgScore;
    double d2Score = avgScore - minScore;

    for (int i = 0; i < n; i++) {
        if (avgScore + 2.0 / 3 * d1Score <= scores[i] && scores[i] <= maxScore) {
            grades[i] = 0;
        }
        else if (avgScore + 1.0 / 3 * d1Score <= scores[i]) {
            grades[i] = 1;
        }
        else if (avgScore <= scores[i]) {
            grades[i] = 2;
        }
        else if (minScore + 1.0 / 5 * d2Score <= scores[i]) {
            grades[i] = 3;
        }
        else {
            grades[i] = 4;
        }
    }
    return;
}

void mpi_calculate_grade_count(double* scores, int n, int** grade_counts, int* no_of_grades, MPI_Comm comm){
    // Pseudo code:
    //      1. calculate grade for each score
    //      2. define/update the no_of_grades available (eg: the assignment requires 5. i.e. A,B,C,D,F)
    //      3. allocate memory for grade_counts and reset the counts to 0
    //      4. calculate local grade counts
    //      5. update grade_counts by calculating the global grade counts
    int* grades = (int*) malloc(sizeof(int) * n);
    mpi_calculate_grades(scores, n, grades, comm);
    *no_of_grades = 5;
    int* local_grade_counts = (int*) malloc(sizeof(int) * (*no_of_grades));
    (*grade_counts) = (int*) malloc(sizeof(int) * (*no_of_grades));

    for (int i = 0; i < (*no_of_grades); i++) {
        local_grade_counts[i] = 0;
    }
    for (int i = 0; i < n; i++) {
        local_grade_counts[grades[i]]++;
    }

    MPI_Allreduce(local_grade_counts, *grade_counts, *no_of_grades, MPI_INT, MPI_SUM, comm);
    // TODO    
    return;
}
